package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String city="";
    EditText City;
    Button Submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        City = findViewById(R.id.city);
        Submit = findViewById(R.id.submit);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Display_Int = new Intent(MainActivity.this,Display.class);
                city = City.getText().toString();
                if(city.equals(""))
                {
                    Toast.makeText(getApplicationContext(),"City cannot be empty", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Display_Int.putExtra("City",city);
                    startActivity(Display_Int);
                    finish();
                }
            }
        });

    }
}